/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.Customer;

import HelperClass.Market.Market;

/**
 *
 * @author Deepak Chandwani
 */
public class Customer {

    int CustomerId;
    String Name;
    String ContactPersonName;
    String ContactPersonCell;
    String Address;
    private Market marketType;
    static int count = 0;

    public Customer() {
        count++;
        setCustomerId(count);
    }

    public String getContactPersonName() {
        return ContactPersonName;
    }

    public void setContactPersonName(String ContactPersonName) {
        this.ContactPersonName = ContactPersonName;
    }

    public String getContactPersonCell() {
        return ContactPersonCell;
    }

    public void setContactPersonCell(String ContactPersonCell) {
        this.ContactPersonCell = ContactPersonCell;
    }

    public Market getMarketType() {
        return marketType;
    }

    public void setMarketType(Market marketType) {
        this.marketType = marketType;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int CustomerId) {
        this.CustomerId = CustomerId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    @Override
    public String toString() {
        return Name;
    }
}
