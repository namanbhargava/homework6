/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.Customer;

import java.util.ArrayList;

/**
 *
 * @author Deepak Chandwani
 */
public class CustomerDirectory {
    
    ArrayList<Customer> customerArrayList;

    public CustomerDirectory() {
        customerArrayList = new ArrayList<>();
    }

    public ArrayList<Customer> getCustomerArrayList() {
        return customerArrayList;
    }

    public void setCustomerArrayList(ArrayList<Customer> customerArrayList) {
        this.customerArrayList = customerArrayList;
    }
    
    
    public Customer addCustomer()
    {
        Customer c = new Customer();
        customerArrayList.add(c);
        return c;
    }
    
    
}
