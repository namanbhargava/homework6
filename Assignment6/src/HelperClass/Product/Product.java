/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.Product;

import HelperClass.Market.MarketOffer;

/**
 *
 * @author Deepak Chandwani
 */
public class Product {
    int ProductId;
    String Name;
    String Description;
    int Availability;
    MarketOffer marketOffer;
    static int count = 0;

    public Product() {
        count++;
        setProductId(count);
    }

    public MarketOffer getMarketOffer() {
        return marketOffer;
    }

    public void setMarketOffer(MarketOffer marketOffer) {
        this.marketOffer = marketOffer;
    }
    
    

    public int getProductId() {
        return ProductId;
    }

    private void setProductId(int ProductId) {
        this.ProductId = ProductId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public int getAvailability() {
        return Availability;
    }

    public void setAvailability(int Availability) {
        this.Availability = Availability;
    }
    
    @Override
    public String toString()
    {
        return  Name;
    }
}
