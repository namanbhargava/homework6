/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.Product;

import java.util.ArrayList;

/**
 *
 * @author Deepak Chandwani
 */
public class ProductDirectory {
    ArrayList<Product> productDirectoryArrayList;

    public ProductDirectory() {
        productDirectoryArrayList = new ArrayList<>();
    }

    public ArrayList<Product> getProductDirectoryArrayList() {
        return productDirectoryArrayList;
    }

    public void setProductDirectoryArrayList(ArrayList<Product> productDirectoryArrayList) {
        this.productDirectoryArrayList = productDirectoryArrayList;
    }
    
    public Product addProduct()
    {
        Product p = new Product();
        productDirectoryArrayList.add(p);
        return p;
    }
    
    public void removeProduct(Product p)
    {
        productDirectoryArrayList.remove(p);
    }
    
    
    
}
