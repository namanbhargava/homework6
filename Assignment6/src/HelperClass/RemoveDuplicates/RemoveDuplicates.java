/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.RemoveDuplicates;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author Deepak Chandwani
 */
public class RemoveDuplicates {
    
    public ArrayList removeDuplicates(ArrayList arrayList)
    {
        Set set = new HashSet();
        ArrayList uniqueItemsList = new ArrayList();
        for (Iterator iterator = arrayList.iterator(); iterator.hasNext(); ) {
          Object object = iterator.next();
          if (set.add(object))
            uniqueItemsList.add(object);
        }
        arrayList.clear();
        arrayList.addAll(uniqueItemsList);
        return arrayList;
    }
    
}
