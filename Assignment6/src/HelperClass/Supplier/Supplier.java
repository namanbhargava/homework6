/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.Supplier;

import HelperClass.Product.ProductDirectory;

/**
 *
 * @author Deepak Chandwani
 */
public class Supplier {
    private int SupplierId;
    private String Name;
    private ProductDirectory productDirectory;
    static int count = 0;

    public Supplier() {
        count++;
        setSupplierId(count);
    }
    
    

    public int getSupplierId() {
        return SupplierId;
    }

    private void setSupplierId(int SupplierId) {
        this.SupplierId = SupplierId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public ProductDirectory getProductDirectory() {
        return productDirectory;
    }

    public void setProductDirectory(ProductDirectory productDirectory) {
        this.productDirectory = productDirectory;
    }
    
    @Override
    public String toString()
    {
      return Name;  
    }
    
    
    
}
