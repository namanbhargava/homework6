/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.Supplier;

import java.util.ArrayList;

/**
 *
 * @author Deepak Chandwani
 */
public class SupplierDirectory {
    
    ArrayList<Supplier> supplierDirectotyArrayList;

    public SupplierDirectory() {
        supplierDirectotyArrayList = new ArrayList<>();
    }

    public ArrayList<Supplier> getSupplierDirectotyArrayList() {
        return supplierDirectotyArrayList;
    }

    public void setSupplierDirectotyArrayList(ArrayList<Supplier> supplierDirectotyArrayList) {
        this.supplierDirectotyArrayList = supplierDirectotyArrayList;
    }
    
    public Supplier addSupplier()
    {
        Supplier s = new Supplier();
        supplierDirectotyArrayList.add(s);
        return s;
    }
    public void addSupplier(Supplier s)
    {
        supplierDirectotyArrayList.add(s);
    }
    
    public void removeSupplier(Supplier s)
    {
        supplierDirectotyArrayList.remove(s);
    }
    
    
}
