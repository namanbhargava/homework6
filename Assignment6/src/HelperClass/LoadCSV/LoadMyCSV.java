/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.LoadCSV;

import Business.Business;
import HelperClass.Customer.Customer;
import HelperClass.Customer.CustomerDirectory;
import HelperClass.Employee.Employee;
import HelperClass.Employee.EmployeeDirectory;
import HelperClass.EmployeeRole.EmployeeRole;
import HelperClass.Encrypt.Encrypt;
import HelperClass.Market.Market;
import HelperClass.Market.MarketPrice;
import HelperClass.Market.MarketOffer;
import HelperClass.Product.Product;
import HelperClass.Product.ProductDirectory;
import HelperClass.Supplier.Supplier;
import HelperClass.Supplier.SupplierDirectory;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Deepak Chandwani
 */
public class LoadMyCSV {

    //String csvPath = "C:\\Users\\bothr\\Documents\\NetBeansProjects\\Homework6_group_3\\Homework6_group\\src\\HelperClass\\CSVFiles\\";
    String csvPath = "C:/Users/bharg/Documents/NetBeansProjects/Assignment6/src/HelperClass/CSVFiles/";

    String line = "";
    String cvsDataSeparator = ",";
    String fileName = null;
    String fileProducts = "Products.csv";
    String fileEmployee = "Employee.csv";
    String fileCustomer = "Customer.csv";
    String fileSupplier = "Supplier.csv";
    //String fileCustomer = "Supplier.csv";
    //String[] product;

    private Business business;

    //private Customer customer;
    private CustomerDirectory customerDirectory;

    //private Employee employee;
    private EmployeeDirectory employeeDirectory;

    //private Supplier supplier;
    private SupplierDirectory supplierDirectory;

    //private Product product;
    private ProductDirectory productDirectory;

    public LoadMyCSV(Business business) {
        this.business = business;
        this.customerDirectory = business.getCustomerDirectory();
        this.employeeDirectory = business.getEmployeeDirectory();
        this.supplierDirectory = business.getSupplierDirectory();
        this.productDirectory = business.getProductDirectory();

        readEmployee();
        readCustomer();
    }

    private void readEmployee() {
        try (BufferedReader br = new BufferedReader(new FileReader(csvPath.concat(fileEmployee)))) {
            int i = 0;
            while ((line = br.readLine()) != null) {
                if (i >= 1) {
                    String[] employee = line.split(cvsDataSeparator);
                    //System.out.println(line);

                    Employee e = employeeDirectory.addEmployee();
                    e.setName(employee[1]);
                    e.setUserName(employee[2]);
                    int p = new Encrypt().encrypt(employee[3]);
                    e.setPassword(p);
                    EmployeeRole er = new EmployeeRole();
                    er.setRole(employee[4]);
                    e.setEmployeeRole(er);
                } else {
                    i++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Something went wrong in loading CSV");
        }
        readSupplier();
    }
    
    private void readCustomer() {
        try (BufferedReader br = new BufferedReader(new FileReader(csvPath.concat(fileCustomer)))) {
            int i = 0;
            while ((line = br.readLine()) != null) {
                if (i >= 1) {
                    String[] customer = line.split(cvsDataSeparator);
                    //System.out.println(line);

                    Customer c = customerDirectory.addCustomer();
                    c.setName(customer[0]);
                    c.setAddress(customer[1]);
                    Market m = new Market();
                    m.setMarketType(customer[2]);
                    c.setMarketType(m);
                    c.setContactPersonName(customer[3]);
                    c.setContactPersonCell(customer[4]); 
                } else {
                    i++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Something went wrong in loading CSV");
        }
    }

    private void readSupplier() {
        //String supplier = null;
        ArrayList<Supplier> tempSuplier = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvPath.concat(fileSupplier)))) {
            int i = 0;
            while ((line = br.readLine()) != null) {
                if (i >= 1) {
                    String[] supplier = line.split(cvsDataSeparator);
                    //System.out.println(line);                    
                    Supplier s = new Supplier();
                    s.setName(supplier[0]);                    
                    tempSuplier.add(s);   
                } else {
                    i++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Something went wrong in loading CSV");
        }
        //tempSuplier = new RemoveDuplicates().removeDuplicates(tempSuplier);
        ArrayList<String> x = new ArrayList<>();
        for (int i = 0; i < tempSuplier.size(); i++) {
            Supplier s = tempSuplier.get(i);
            if (!(x.contains(s.getName()))) {
                supplierDirectory.addSupplier(s);
                //System.out.println("Supplier s " + s.getName());
            }
        }
        readSupplierProduct();
    }

    private void readSupplierProduct() {

        //System.out.println("Supplier size " + String.valueOf(business.getSupplierDirectory().getSupplierDirectotyArrayList().size()));
        for (Supplier s : business.getSupplierDirectory().getSupplierDirectotyArrayList()) {
            s.setProductDirectory(readProduct(s));
        }
    }

    private ProductDirectory readProduct(Supplier supplier) {
        //String supplier = null;
        ProductDirectory pD = new ProductDirectory();
        ArrayList<Product> tempProduct = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvPath.concat(fileProducts)))) {
            int i = 0;
            tempProduct.clear();
            while ((line = br.readLine()) != null) {
                if (i >= 1) {
                    String[] product = line.split(cvsDataSeparator);
                    
                    if (product[0].equalsIgnoreCase(supplier.getName())) {
                        Product p = business.getProductDirectory().addProduct();
                        p.setName(product[2]);
                        p.setAvailability(Integer.parseInt(product[3]));
                        p.setDescription(product[7]);

                        MarketOffer m = new MarketOffer();

                        ///Education MarketPrice
                        MarketPrice mkt = m.addEducationMarketOffer();
                        mkt.setFloorPrice(Double.parseDouble(product[4]));
                        mkt.setCeilPrice(Double.parseDouble(product[5]));
                        mkt.setTargetPrice(Double.parseDouble(product[6]));

                        ///// Finance MarketPrice
                        mkt = m.addFinanceMarketOffer();
                        mkt.setFloorPrice(Double.parseDouble(product[8]));
                        mkt.setCeilPrice(Double.parseDouble(product[9]));
                        mkt.setTargetPrice(Double.parseDouble(product[10]));

                        p.setMarketOffer(m);
                        tempProduct.add(p);
                    }
                } else {
                    i++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Something went wrong in loading CSV");
        }
        //System.out.println("tempProduct size " + String.valueOf(tempProduct.size()));
        pD.setProductDirectoryArrayList(tempProduct);
        return pD;
    }
}
