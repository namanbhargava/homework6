/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.Employee;

import HelperClass.EmployeeRole.EmployeeRole;

/**
 *
 * @author Deepak Chandwani
 */
public class Employee {
    private int EmployeeId;
    private String Name;
    private String userName;
    private EmployeeRole employeeRole;
    private int Password;
    static int count = 0;

    public Employee() {
        count++;
        setEmployeeId(count);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    

    public int getPassword() {
        return Password;
    }

    public void setPassword(int Password) {
        this.Password = Password;
    }
    
    
    
    public int getEmployeeId() {
        return EmployeeId;
    }

    private void setEmployeeId(int EmployeeId) {
        this.EmployeeId = EmployeeId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public EmployeeRole getEmployeeRole() {
        return employeeRole;
    }

    public void setEmployeeRole(EmployeeRole employeeRole) {
        this.employeeRole = employeeRole;
    }
    
    
    
}
