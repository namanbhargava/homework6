/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.Employee;

import java.util.ArrayList;

/**
 *
 * @author Deepak Chandwani
 */
public class EmployeeDirectory {

    ArrayList<Employee> employeeArrayList;

    public EmployeeDirectory() {
        employeeArrayList = new ArrayList<>();
    }

    public ArrayList<Employee> getEmployeeArrayList() {
        return employeeArrayList;
    }

    public void setEmployeeArrayList(ArrayList<Employee> employeeArrayList) {
        this.employeeArrayList = employeeArrayList;
    }

    public Employee addEmployee() {
        Employee e = new Employee();
        employeeArrayList.add(e);
        return e;
    }

    public void removeEmployee(Employee e) {
        employeeArrayList.remove(e);
    }

    

}
