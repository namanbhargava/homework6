/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.Order;

import HelperClass.Product.Product;

/**
 *
 * @author Deepak Chandwani
 */
public class OrderItem {
    private Product product;
    private int Quantity;
    private double itemPaidPrice;
    private double itemTargetPrice;
    private double commission;
    private double itemTotalPrice;

    public double getItemTotalPrice() {
        return itemTotalPrice;
    }

    public void setItemTotalPrice(double itemTotalPrice) {
        this.itemTotalPrice = itemTotalPrice;
    }
    
    

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }
    
    

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int Quantity) {
        this.Quantity = Quantity;
    }

    public double getItemPaidPrice() {
        return itemPaidPrice;
    }

    public void setItemPaidPrice(double itemPaidPrice) {
        this.itemPaidPrice = itemPaidPrice;
    }

    public double getItemTargetPrice() {
        return itemTargetPrice;
    }

    public void setItemTargetPrice(double itemTargetPrice) {
        this.itemTargetPrice = itemTargetPrice;
    }
    
    
    
    
}
