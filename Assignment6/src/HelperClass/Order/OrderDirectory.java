/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.Order;

import java.util.ArrayList;

/**
 *
 * @author Deepak Chandwani
 */
public class OrderDirectory {
    ArrayList<OrderItem> orderArrayList;

    public OrderDirectory() {
        orderArrayList = new ArrayList<>();
    }

    public ArrayList<OrderItem> getOrderArrayList() {
        return orderArrayList;
    }

    public void setOrderArrayList(ArrayList<OrderItem> orderArrayList) {
        this.orderArrayList = orderArrayList;
    }
}
