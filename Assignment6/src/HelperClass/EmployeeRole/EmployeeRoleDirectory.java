/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.EmployeeRole;

import java.util.ArrayList;

/**
 *
 * @author Deepak Chandwani
 */
public class EmployeeRoleDirectory {
    ArrayList<EmployeeRole> employeeRoleArrayList;

    public EmployeeRoleDirectory() {
        employeeRoleArrayList = new ArrayList<>();
    }

    public ArrayList<EmployeeRole> getEmployeeRoleArrayList() {
        return employeeRoleArrayList;
    }

    public void setEmployeeRoleArrayList(ArrayList<EmployeeRole> employeeRoleArrayList) {
        this.employeeRoleArrayList = employeeRoleArrayList;
    }
    
    public EmployeeRole addEmployeeRole()
    {
        EmployeeRole eR = new EmployeeRole();
        employeeRoleArrayList.add(eR);
        return eR;
    }
    
    public void removeEmployeeRole(EmployeeRole eR)
    {
        employeeRoleArrayList.remove(eR);
    }
    
    
    
}
