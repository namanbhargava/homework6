/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.EmployeeWorkArea;

import Business.Business;
import HelperClass.Customer.Customer;
import HelperClass.Market.MarketPrice;
import HelperClass.Order.OrderItem;
import HelperClass.Product.Product;
import HelperClass.Supplier.Supplier;
import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author bharg
 */
public class BookCustomerOrderJPanel extends javax.swing.JPanel {

    /**
     * Creates new form OrderBookingJPanel
     */
    private Business business;
    private JPanel upc;
    private Customer customer;
    //private String mktType = "Education";
    private String mktType;// = "Finance";
    private double orderTotal = 0;
    private double commission = 0;
    ArrayList<OrderItem> cart;
    Supplier supplierSelected;

    public BookCustomerOrderJPanel(JPanel upc, Business business, Customer customer) {
        initComponents();
        this.upc = upc;
        this.business = business;
        this.customer = customer;
        cart = new ArrayList<>();
        cart.clear();

        supplierSelected = (Supplier) comboBoxSupplier.getSelectedItem();
        mktType = customer.getMarketType().getMarketType();
        jTextFieldCommision.setText(String.valueOf(commission));
        jLabelCartTotal.setText(String.valueOf(orderTotal));

        for (Supplier s : business.getSupplierDirectory().getSupplierDirectotyArrayList()) {
            comboBoxSupplier.addItem(s);
        }
    }

    public void populatetable(Supplier supplierSelected) {
        DefaultTableModel dtm = (DefaultTableModel) tableSupplierProductCatalog.getModel();
        dtm.setRowCount(0);
        for (Product p : supplierSelected.getProductDirectory().getProductDirectoryArrayList()) {
            Object[] obj = new Object[6];
            obj[0] = p;
            obj[1] = p.getProductId();
            if (mktType.equalsIgnoreCase("Education")) {
                for (MarketPrice m : p.getMarketOffer().getEducationMarketOfferArrayList()) {
                    obj[3] = m.getCeilPrice();
                    obj[2] = m.getFloorPrice();
                    obj[4] = m.getTargetPrice();
                }
            } else {
                for (MarketPrice m : p.getMarketOffer().getFinanceMarketOfferArrayList()) {
                    obj[3] = m.getCeilPrice();
                    obj[2] = m.getFloorPrice();
                    obj[4] = m.getTargetPrice();
                }
            }
            obj[5] = p.getAvailability();
            dtm.addRow(obj);
        }
    }

    public void populateCart() {
        DefaultTableModel dtm = (DefaultTableModel) jTableCart.getModel();
        dtm.setRowCount(0);
        for (OrderItem orderItem : cart) {
            Object[] obj = new Object[5];
            obj[0] = orderItem.getProduct().getName();
            obj[1] = orderItem.getItemPaidPrice();
            obj[2] = orderItem.getQuantity();
            obj[3] = orderItem.getItemTotalPrice();
            obj[4] = orderItem;
            dtm.addRow(obj);
        }
        populatetable(supplierSelected);
    }

    public void updateProductAvailability(Product product, Supplier supplierSelected, int availability) {
        for (Product p : supplierSelected.getProductDirectory().getProductDirectoryArrayList()) {
            if (product.equals(p)) {
                p.setAvailability(availability);
            }
        }
        updateUserInterface();
    }

    public void removeCartItem(OrderItem orderItem) {

//        for (OrderItem oi : cart) {
        for (int i = 0; i< cart.size(); i++) {
            OrderItem oi = cart.get(i);
            if (oi.equals(orderItem)) {
                Product product = orderItem.getProduct();
                for (Product p : supplierSelected.getProductDirectory().getProductDirectoryArrayList()) {
                    if (p.equals(product)) {
                        p.setAvailability(p.getAvailability() + oi.getQuantity());
                        orderTotal -= oi.getItemTotalPrice();
                        if (oi.getCommission() >= 0) {
                            commission -= oi.getCommission();
                        } else if (oi.getCommission() < 0) {
                            commission += oi.getCommission();
                        }
                    }
                }
            }
        }
        cart.remove(orderItem);
        populateCart();
        updateUserInterface();        
    }
    public void updateUserInterface()
    {
        System.out.println("orderTotal : "+String.valueOf(orderTotal));
        jLabelCartTotal.setText(String.valueOf(orderTotal));
        
        jTextFieldCommision.setText(String.valueOf(commission));
        System.out.println("orderTotal : "+String.valueOf(commission));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        comboBoxSupplier = new javax.swing.JComboBox();
        txtSearchKeyWord = new javax.swing.JTextField();
        btnSearchProduct = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableSupplierProductCatalog = new javax.swing.JTable();
        viewProdjButton2 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldSalesPrice = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jSpinnerQuantity = new javax.swing.JSpinner();
        addtoCartButton6 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableCart = new javax.swing.JTable();
        btnCheckOut = new javax.swing.JButton();
        btnRemoveOrderItem = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldCommision = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabelCartTotal = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setText("Supplier");

        comboBoxSupplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxSupplierActionPerformed(evt);
            }
        });

        btnSearchProduct.setText("Search Product");
        btnSearchProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchProductActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Supplier Product Catalog");

        tableSupplierProductCatalog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Product Id", "Floor Price", "Ceiling Price", "Target Price", "Availability"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableSupplierProductCatalog);

        viewProdjButton2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        viewProdjButton2.setText("View Product Detail");
        viewProdjButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewProdjButton2ActionPerformed(evt);
            }
        });

        jLabel6.setText("Sales Price:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Quantity:");

        jSpinnerQuantity.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));

        addtoCartButton6.setText("ADD TO CART");
        addtoCartButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addtoCartButton6ActionPerformed(evt);
            }
        });

        jTableCart.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Item Name", "Price", "Quantity", "Total Amount", "Object"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableCart.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(jTableCart);
        if (jTableCart.getColumnModel().getColumnCount() > 0) {
            jTableCart.getColumnModel().getColumn(4).setMinWidth(0);
            jTableCart.getColumnModel().getColumn(4).setPreferredWidth(0);
            jTableCart.getColumnModel().getColumn(4).setMaxWidth(0);
        }

        btnCheckOut.setText("Check out");
        btnCheckOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckOutActionPerformed(evt);
            }
        });

        btnRemoveOrderItem.setText("Remove");
        btnRemoveOrderItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveOrderItemActionPerformed(evt);
            }
        });

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("My Commision:");

        jTextFieldCommision.setEnabled(false);
        jTextFieldCommision.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCommisionActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Cart");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Customer Order Booking");

        jLabel9.setFont(new java.awt.Font("Tw Cen MT", 0, 24)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 0, 0));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("XEROX");

        jLabelCartTotal.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        jLabelCartTotal.setText("jLabel10");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Cart Total :");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(comboBoxSupplier, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 259, Short.MAX_VALUE))
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtSearchKeyWord, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextFieldCommision)
                            .addComponent(btnSearchProduct, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(viewProdjButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextFieldSalesPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSpinnerQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(addtoCartButton6))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 456, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel4)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnRemoveOrderItem, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnCheckOut, javax.swing.GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE)
                            .addComponent(jLabelCartTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(comboBoxSupplier, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(4, 4, 4)
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jTextFieldCommision, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(4, 4, 4)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtSearchKeyWord, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSearchProduct))))
                .addGap(7, 7, 7)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(viewProdjButton2)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldSalesPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jSpinnerQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel5))
                    .addComponent(addtoCartButton6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addGap(12, 12, 12)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelCartTotal)
                    .addComponent(jLabel10))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRemoveOrderItem)
                    .addComponent(btnBack)
                    .addComponent(btnCheckOut))
                .addContainerGap(20, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void comboBoxSupplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxSupplierActionPerformed
        // TODO add your handling code here:
        supplierSelected = (Supplier) comboBoxSupplier.getSelectedItem();
        populatetable(supplierSelected);
    }//GEN-LAST:event_comboBoxSupplierActionPerformed

    private void btnSearchProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchProductActionPerformed

    }//GEN-LAST:event_btnSearchProductActionPerformed

    private void viewProdjButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewProdjButton2ActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_viewProdjButton2ActionPerformed

    private void addtoCartButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addtoCartButton6ActionPerformed
        // TODO add your handling code here:

        int checkProductSelected = 0, checkPaidPrice = 0, checkQuantity = 0, checkAvailability = 0;
        boolean productFound = false;

        if (tableSupplierProductCatalog.getSelectedRow() >= 0) {
            checkProductSelected = 1;
        } else {
            JOptionPane.showMessageDialog(null, "Please select a product to add it to cart");
            checkProductSelected = 0;
        }

        if (jTextFieldSalesPrice.getText().trim().length() > 0) {
            checkPaidPrice = 1;
        } else {
            checkPaidPrice = 0;
            JOptionPane.showMessageDialog(null, "Price cannot be empty");
        }
        int quantity = (Integer) jSpinnerQuantity.getValue();
        if (quantity > 0) {
            checkQuantity = 1;
        } else {
            JOptionPane.showMessageDialog(null, "Quantity cannot be smaller than 1");
            checkQuantity = 0;
        }

        if (checkQuantity == 1 && checkPaidPrice == 1 && checkProductSelected == 1) {
            Product p = (Product) tableSupplierProductCatalog.getValueAt(tableSupplierProductCatalog.getSelectedRow(), 0);
            //if (p.getAvailability() >= quantity) {
            double itemPaidPrice = 0;
            try {
                itemPaidPrice = Double.parseDouble(jTextFieldSalesPrice.getText().trim());
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Only Integers Numbers allowed");
            }
            double targetPrice = (double) tableSupplierProductCatalog.getValueAt(tableSupplierProductCatalog.getSelectedRow(), 4);

            for (OrderItem oi : cart) {
                if (oi.getProduct().equals(p)) {
                    productFound = true;

                    int oldQuantity = oi.getQuantity();
                    int oldAvailability = p.getAvailability();
                    int newAvailability = ((oldAvailability + oldQuantity) - quantity);

                    System.out.println("oldQuantity : " + String.valueOf(oldQuantity));
                    System.out.println("oldAvailability : " + String.valueOf(oldAvailability));
                    System.out.println("newAvailability : " + String.valueOf(newAvailability));
                    System.out.println("result  : " + ((newAvailability >= quantity) || (oldQuantity >= quantity)));

                    if (newAvailability >= quantity || oldQuantity >= quantity) {
                        updateProductAvailability(p, supplierSelected, newAvailability);
                        orderTotal -= oi.getItemTotalPrice();
                        if (oi.getCommission() >= 0) {
                            System.out.println("if (oi.getCommission() >= 0) : " + String.valueOf(commission));
                            commission -= oi.getCommission();
                            System.out.println("if (oi.getCommission() >= 0) : " + String.valueOf(commission));
                        } else if (oi.getCommission() < 0) {
                            System.out.println("if (oi.getCommission() < 0) : " + String.valueOf(commission));
                            commission += oi.getCommission();
                            System.out.println("if (oi.getCommission() < 0) : " + String.valueOf(commission));

                        }

                        oi.setItemPaidPrice(itemPaidPrice);
                        oi.setItemTargetPrice(targetPrice);
                        oi.setQuantity(quantity);
                        oi.setItemTotalPrice(oi.getItemPaidPrice() * oi.getQuantity());
                        oi.setProduct(p);
                        orderTotal += oi.getItemTotalPrice();

                        if (targetPrice > itemPaidPrice) {
                            commission += (itemPaidPrice - targetPrice) * quantity * (0.10);
                            oi.setCommission((itemPaidPrice - targetPrice) * quantity * (0.10));
                        } else {
                            commission += (itemPaidPrice - targetPrice) * quantity * (0.10);
                            oi.setCommission((itemPaidPrice - targetPrice) * quantity * (0.10));
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "ordered qty cannot be more than availability (newAvailability >= quantity || oldQuantity >= quantity)");
                    }
                }
            }

            if (!productFound) {
                if (p.getAvailability() >= quantity) {

                    OrderItem orderItem = new OrderItem();

                    orderItem.setItemPaidPrice(itemPaidPrice);
                    orderItem.setItemTargetPrice(targetPrice);
                    orderItem.setQuantity(quantity);
                    orderItem.setItemTotalPrice(orderItem.getItemPaidPrice() * orderItem.getQuantity());
                    orderItem.setProduct(p);
                    orderTotal += orderItem.getItemTotalPrice();
                    int newAvailability = p.getAvailability() - quantity;
                    updateProductAvailability(p, supplierSelected, newAvailability);
                    if (targetPrice > itemPaidPrice) {
                        commission += (itemPaidPrice - targetPrice) * quantity * (0.10);
                        orderItem.setCommission((itemPaidPrice - targetPrice) * quantity * (0.10));
                    } else {
                        commission += (itemPaidPrice - targetPrice) * quantity * (0.10);
                        orderItem.setCommission((itemPaidPrice - targetPrice) * quantity * (0.10));
                    }
                    cart.add(orderItem);
//                    } else {
//                        JOptionPane.showMessageDialog(null, "ordered qty cannot be more than availability");
//                    }
                } else {
                    JOptionPane.showMessageDialog(null, "ordered qty cannot be more than availability");
                }

                ///// check availability of product
            }
        }
        populateCart();
        updateUserInterface();
        jTextFieldSalesPrice.setText("");
        jSpinnerQuantity.setValue(0);
    }//GEN-LAST:event_addtoCartButton6ActionPerformed

    private void btnCheckOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckOutActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_btnCheckOutActionPerformed

    private void btnRemoveOrderItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveOrderItemActionPerformed

        ///remove button
        if (jTableCart.getSelectedRow() >= 0) {
            OrderItem orderItem = (OrderItem) jTableCart.getValueAt(jTableCart.getSelectedRow(), 4);
            removeCartItem(orderItem);
        }
    }//GEN-LAST:event_btnRemoveOrderItemActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        upc.remove(this);
        CardLayout layout = (CardLayout) upc.getLayout();
        layout.previous(upc);
    }//GEN-LAST:event_btnBackActionPerformed

    private void jTextFieldCommisionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCommisionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldCommisionActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addtoCartButton6;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCheckOut;
    private javax.swing.JButton btnRemoveOrderItem;
    private javax.swing.JButton btnSearchProduct;
    private javax.swing.JComboBox comboBoxSupplier;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelCartTotal;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSpinner jSpinnerQuantity;
    private javax.swing.JTable jTableCart;
    private javax.swing.JTextField jTextFieldCommision;
    private javax.swing.JTextField jTextFieldSalesPrice;
    private javax.swing.JTable tableSupplierProductCatalog;
    private javax.swing.JTextField txtSearchKeyWord;
    private javax.swing.JButton viewProdjButton2;
    // End of variables declaration//GEN-END:variables
}
