/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import HelperClass.Customer.Customer;
import HelperClass.Customer.CustomerDirectory;
import HelperClass.Employee.Employee;
import HelperClass.Employee.EmployeeDirectory;
import HelperClass.LoadCSV.LoadMyCSV;
import HelperClass.MasterOrder.MasterOrderDirectory;
import HelperClass.Order.OrderDirectory;
import HelperClass.Order.OrderItem;
import HelperClass.Product.Product;
import HelperClass.Product.ProductDirectory;
import HelperClass.Supplier.Supplier;
import HelperClass.Supplier.SupplierDirectory;

/**
 *
 * @author Deepak Chandwani
 */
public class Business {
    private Customer customer;
    private CustomerDirectory customerDirectory;
    
    private Employee employee;
    private EmployeeDirectory employeeDirectory; 
    
    private Supplier supplier;
    private SupplierDirectory supplierDirectory;    
    
    private Product product;
    private ProductDirectory productDirectory;
    
    private OrderItem orderItem;
    private OrderDirectory orderDirectory;
    private MasterOrderDirectory masterOrderDirectory;
    
    

    public Business() {
        customer = new Customer();
        setCustomer(customer);
        customerDirectory = new CustomerDirectory();
        setCustomerDirectory(customerDirectory);
        
        employee = new Employee();
        setEmployee(employee);
        employeeDirectory = new EmployeeDirectory();
        setEmployeeDirectory(employeeDirectory);
        
        supplier = new Supplier();
        setSupplier(supplier);
        supplierDirectory = new SupplierDirectory();
        setSupplierDirectory(supplierDirectory);
        
        product = new Product();
        setProduct(product);
        productDirectory = new ProductDirectory();
        setProductDirectory(productDirectory);
        
        orderItem = new OrderItem();
        setOrderItem(orderItem);
        orderDirectory = new OrderDirectory();
        setOrderDirectory(orderDirectory);
        masterOrderDirectory = new MasterOrderDirectory();
        setMasterOrderDirectory(masterOrderDirectory);
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    public void setEmployeeDirectory(EmployeeDirectory employeeDirectory) {
        this.employeeDirectory = employeeDirectory;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public SupplierDirectory getSupplierDirectory() {
        return supplierDirectory;
    }

    public void setSupplierDirectory(SupplierDirectory supplierDirectory) {
        this.supplierDirectory = supplierDirectory;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ProductDirectory getProductDirectory() {
        return productDirectory;
    }

    public void setProductDirectory(ProductDirectory productDirectory) {
        this.productDirectory = productDirectory;
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }

    public OrderDirectory getOrderDirectory() {
        return orderDirectory;
    }

    public void setOrderDirectory(OrderDirectory orderDirectory) {
        this.orderDirectory = orderDirectory;
    }

    public MasterOrderDirectory getMasterOrderDirectory() {
        return masterOrderDirectory;
    }

    public void setMasterOrderDirectory(MasterOrderDirectory masterOrderDirectory) {
        this.masterOrderDirectory = masterOrderDirectory;
    }
    
    
    
    
    
}
